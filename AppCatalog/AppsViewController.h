//
//  AppsViewController.h
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 8/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppCategory.h"

@interface AppsViewController : UIViewController
@property AppCategory *category;
@property (nonatomic, retain) NSMutableArray *allCategories;
@end
