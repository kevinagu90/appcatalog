//
//  SplashViewController.m
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 8/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "SplashViewController.h"
#import "AppService.h"
#import <JTMaterialSpinner/JTMaterialSpinner.h>

NSString *const segueName = @"toCategories";
@interface SplashViewController ()
@property (weak, nonatomic) IBOutlet JTMaterialSpinner *loagingView;

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar setHidden:YES];
    [self fetchAppsFromService];
    _loagingView.circleLayer.lineWidth = 2.0;
    
    // Change the color of the line
    _loagingView.circleLayer.strokeColor = [UIColor grayColor].CGColor;
    [_loagingView beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*!Call Appservice class to fectch all apps*/
-(void)fetchAppsFromService{
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(queue, ^{
        [AppService fetchAppsWithCompletion:^{
            [_loagingView endRefreshing];
            [self performSegueWithIdentifier:segueName sender:nil];
        }];
    });
}

@end
