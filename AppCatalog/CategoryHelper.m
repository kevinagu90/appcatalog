//
//  CategoryHelper.m
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 9/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "CategoryHelper.h"

@implementation CategoryHelper

+(NSArray*)getAppsByCategory:(AppCategory*)category{
    NSMutableArray * array = [[NSMutableArray alloc] init];
     RLMResults *result = [App objectsWhere:@"category == %@", category];
     for (RLMObject *object in result) {
     [array addObject:(App*)object];
     }
    return array;
}
@end
