//
//  DescriptionViewCell.m
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 8/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "DescriptionViewCell.h"
@interface DescriptionViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end

@implementation DescriptionViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/*!Set all view element of cell.*/
-(void)configureCell{
    _descriptionLabel.text = _descript;
}

@end
