//
//  DetailViewController.m
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 8/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "DetailViewController.h"
#import "GlobalVariables.h"
#import "DescriptionViewCell.h"
#import "AppViewCell.h"

@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UITableView *detailTableView;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_detailTableView setRowHeight:UITableViewAutomaticDimension];
    [_detailTableView setEstimatedRowHeight:DEFAULT_CELL_HEIGHT];
       [self.detailTableView registerNib:[UINib nibWithNibName:APP_NIB_NAME bundle:nil] forCellReuseIdentifier:APP_CELL];
      [self.detailTableView registerNib:[UINib nibWithNibName:DESC_NIB_NAME bundle:nil] forCellReuseIdentifier:DESC_CELL];
    [self.detailTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return TOTAL_ROWS_DESC_TABLE;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{    AppViewCell *cell = [tableView dequeueReusableCellWithIdentifier:APP_CELL];
    DescriptionViewCell *descCell = [tableView dequeueReusableCellWithIdentifier:DESC_CELL];
    
    switch (indexPath.row) {
        case 0:
            [cell setApp:_app];
            [cell configureCell];
            return cell;
            break;
        case 1:
            [descCell setDescript:[_app summary]];
            [descCell configureCell];
            return descCell;
            break;
            
        default:
            [descCell setDescript:[_app rights]];
            [descCell configureCell];
            return descCell;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    return  indexPath.row ==0? DEFAULT_CELL_HEIGHT : UITableViewAutomaticDimension;
}

@end
