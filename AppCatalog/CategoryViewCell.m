//
//  CategoryViewCell.m
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 8/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "CategoryViewCell.h"

NSString *const categoryImage = @"defaultIcon";
@interface CategoryViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *categoryImage;
@property (weak, nonatomic) IBOutlet UILabel *categoryNameLabel;
@end

@implementation CategoryViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell{
    _categoryImage.image = [UIImage imageNamed:categoryImage];
    _categoryNameLabel.text = [_category name];
}

@end
