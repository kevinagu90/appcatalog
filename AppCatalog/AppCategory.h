//
//  AppCategory.h
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 8/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import <Realm/Realm.h>


@interface AppCategory : RLMObject
@property NSString *idCategory;
@property NSString *name;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
