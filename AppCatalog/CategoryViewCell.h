//
//  CategoryViewCell.h
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 8/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppCategory.h"

@interface CategoryViewCell : UITableViewCell
@property AppCategory * category;

/*!Set all view element of cell.*/
-(void)configureCell;
@end
