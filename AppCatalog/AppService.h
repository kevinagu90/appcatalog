//
//  AppService.h
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 8/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppCategory.h"

typedef void (^categoriesCompletion)(NSArray <AppCategory*>*categories);
//typedef void (^AppsCompletion)(NSArray <App*>*apps);
@interface AppService : NSObject

/*!Do request to itunes service to fetch apps.*/
+(void)fetchAppsWithCompletion:(void (^)())completion;

/*!Get all categories from local database*/
+(void)fetchCategoriesFromLocal:(categoriesCompletion)completion;

/*!Get all apps from local database*/
//+(void)fetchAppsFromLocal:(AppsCompletion)completion;
@end
