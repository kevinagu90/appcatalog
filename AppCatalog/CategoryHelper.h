//
//  CategoryHelper.h
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 9/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "App.h"

@interface CategoryHelper : NSObject

/*!Do query in realm database for get all apps by this category*/
+(NSArray*)getAppsByCategory:(AppCategory*)category;
@end
