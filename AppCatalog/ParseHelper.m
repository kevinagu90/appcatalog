//
//  ParseHelper.m
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 8/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "ParseHelper.h"
#import "App.h"

@implementation ParseHelper
+(void)parseArrayFromRequest:(NSArray *)array{
   RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    for (int i=0; i<[array count]; i++) {
        App *app = [[App alloc] initWithDictionary:array[i]];
        [App createOrUpdateInDefaultRealmWithValue:app];
    }
    [realm commitWriteTransaction];
}
@end
