//
//  App.m
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 8/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "App.h"

@implementation App
-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    self.idApp = dictionary[@"id"][@"attributes"][@"im:id"];
    self.name = dictionary[@"im:name"][@"label"];
    self.summary = dictionary[@"summary"][@"label"];
    self.price = dictionary[@"im:price"][@"attributes"][@"amount"];
    self.image = [[dictionary[@"im:image"] lastObject] objectForKey:@"label"];
    self.releaseDate = dictionary[@"im:releaseDate"][@"attributes"][@"label"];
    self.rights = dictionary[@"rights"][@"label"];
    self.link = dictionary[@"link"][@"attributes"][@"href"];
    self.artist = dictionary[@"im:artist"][@"label"];
    self.category = [[AppCategory alloc] initWithDictionary:dictionary[@"category"]];
    return self;
}

+ (NSString*)primaryKey {
    return @"idApp";
}

@end
RLM_ARRAY_TYPE(App)