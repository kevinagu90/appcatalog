//
//  AppViewCell.m
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 8/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "AppViewCell.h"

@interface AppViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *appImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *autorLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;


@end
@implementation AppViewCell

- (void)awakeFromNib {
    _appImageView.clipsToBounds = YES;
    _appImageView.layer.cornerRadius = 10;
    _priceView.clipsToBounds = YES;
    _priceView.layer.cornerRadius =5;
    _priceView.layer.borderWidth =2;
    _priceView.layer.borderColor = [UIColor blueColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCell{
    _nameLabel.text = [_app name];
    _autorLabel.text = [_app artist];
    _priceView.text = [self getPriceString];
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[_app image]]];
    _appImageView.image = [UIImage imageWithData:imageData];
}

/*!Convert price in string for be showed.*/
-(NSString*)getPriceString{
    if ((BOOL)[_app.price containsString:@"0.00"]) {
        return @"FREE";
    }else{
        return [NSString stringWithFormat:@"$%@", _app.price];
    }
    return nil;
}
@end
