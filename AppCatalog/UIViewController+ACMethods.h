//
//  UIViewController+ACMethods.h
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 9/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (ACMethods)

/*!Custom navigarion bar for CategoriesViewController*/
-(void)setCategoriesViewNavigationBar;
@end
