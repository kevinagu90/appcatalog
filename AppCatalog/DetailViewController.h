//
//  DetailViewController.h
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 8/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "App.h"

@interface DetailViewController : UIViewController
@property App *app;
@end
