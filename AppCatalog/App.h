//
//  App.h
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 8/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import <Realm/Realm.h>
#import "AppCategory.h"

@interface App : RLMObject
@property NSString *idApp;
@property NSString *name;
@property NSString *summary;
@property NSString *price;
@property NSString *image;
@property AppCategory *category;
@property NSString *rights;
@property NSString *link;
@property NSString *artist;
@property NSString *releaseDate;

-(instancetype)initWithDictionary:(NSDictionary*)dictionary;
@end
