//
//  AppService.m
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 8/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "AppService.h"
#import "GlobalVariables.h"
#import "AFNetworking.h"
#import "ParseHelper.h"

@implementation AppService
+(void)fetchAppsWithCompletion:(void (^)())completion{
    

    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [manager GET:API_URL parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSArray *array = responseObject[@"feed"][@"entry"];
        [ParseHelper parseArrayFromRequest:array];
        completion();
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completion();
    }];
}

+(void)fetchCategoriesFromLocal:(categoriesCompletion)completion{
    NSMutableArray <AppCategory*>*array = [[NSMutableArray alloc] init] ;
    RLMResults *results = [AppCategory allObjects];
    for (RLMObject *object in results) {
        [array addObject:(AppCategory*)object];
    }

    completion(array);
}
@end
