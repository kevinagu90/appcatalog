//
//  GlobalVariables.m
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 8/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "GlobalVariables.h"

@implementation GlobalVariables
NSString *const API_URL = @"https://itunes.apple.com/us/rss/topfreeapplications/limit=20/json";
NSString *const CATEGORY_CELL = @"categoryCell";
NSString *const APP_CELL = @"appCell";
NSString *const CATEGORY_NIB_NAME =@"CategoryViewCell";
NSString *const APP_NIB_NAME =@"AppViewCell";
NSString *const DESC_NIB_NAME =@"DescriptionViewCell";
NSString *const DESC_CELL =@"descriptionCell";
NSInteger const TOTAL_ROWS_DESC_TABLE = 3;
float const DEFAULT_CELL_HEIGHT = 79.0f;
@end
