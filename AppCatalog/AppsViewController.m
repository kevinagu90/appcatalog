//
//  AppsViewController.m
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 8/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "AppsViewController.h"
#import "GlobalVariables.h"
#import "AppViewCell.h"
#import "CategoryHelper.h"
#import "DetailViewController.h"

NSString *const detailSegue = @"toAppDetail";
@interface AppsViewController ()
@property (weak, nonatomic) IBOutlet UITableView *appTableView;
@property (weak, nonatomic) IBOutlet UITableView *categoriesPieckerView;
@property (nonatomic, retain) NSArray *appsArray;
@end

@implementation AppsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [self.appTableView registerNib:[UINib nibWithNibName:APP_NIB_NAME bundle:nil] forCellReuseIdentifier:APP_CELL];
    [self getAppsOfCategory:_category];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*!Get apps group for this category.*/
-(void)getAppsOfCategory:(AppCategory*)category{
    
       _appsArray = [CategoryHelper getAppsByCategory:category];
        [_appTableView reloadData];
        [_categoriesPieckerView reloadData];
   
}

#pragma mark-UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_appsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppViewCell *cell = [tableView dequeueReusableCellWithIdentifier:APP_CELL];
    App *app = [_appsArray objectAtIndex:indexPath.row];
    [cell setApp:app];
    [cell configureCell];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.appTableView deselectRowAtIndexPath:indexPath animated:NO];
    [self performSegueWithIdentifier:detailSegue sender:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return DEFAULT_CELL_HEIGHT;
}

#pragma mark-PickerView
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [_allCategories count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    AppCategory * category = [_allCategories objectAtIndex:row];
    return [category name];
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    AppCategory *category = [_allCategories objectAtIndex:row];
    [self getAppsOfCategory:category];
    
}

/*!Send selecte app to detail view */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:detailSegue]) {
        NSIndexPath *indexPath = (NSIndexPath *)sender;
        // Get reference to the destination view controller
        DetailViewController *vc = [segue destinationViewController];
        App * selectedApp = [_appsArray objectAtIndex:indexPath.row];
        [vc setApp:selectedApp];
    }
}

@end
