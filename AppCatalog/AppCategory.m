//
//  AppCategory.m
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 8/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "AppCategory.h"


@implementation AppCategory

-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    self.idCategory = dictionary[@"attributes"][@"im:id"];
    self.name = dictionary[@"attributes"][@"label"];
    return self;
}

+ (NSString*)primaryKey {
    return @"idCategory";
}


@end
RLM_ARRAY_TYPE(AppCategory)