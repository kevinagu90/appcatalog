//
//  UIViewController+ACMethods.m
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 9/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "UIViewController+ACMethods.h"

@implementation UIViewController (ACMethods)
-(void)setCategoriesViewNavigationBar{
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController.navigationBar setHidden:NO];
}

@end
