//
//  GlobalVariables.h
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 8/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlobalVariables : NSObject
extern NSString *const API_URL;
extern NSString *const CATEGORY_CELL;
extern NSString *const APP_CELL;
extern NSString *const CATEGORY_NIB_NAME;
extern NSString *const APP_NIB_NAME;
extern NSString *const DESC_NIB_NAME;
extern NSString *const DESC_CELL;
extern NSInteger const TOTAL_ROWS_DESC_TABLE;
extern float const DEFAULT_CELL_HEIGHT;
@end
