//
//  ParseHelper.h
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 8/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParseHelper : NSObject

//Parse all apps from request.
+(void)parseArrayFromRequest:(NSArray*)array;
@end
