//
//  CategoriesViewController.m
//  AppCatalog
//
//  Created by kevin Agudelo Betancourt on 8/04/16.
//  Copyright © 2016 kevin Agudelo Betancourt. All rights reserved.
//

#import "CategoriesViewController.h"
#import "GlobalVariables.h"
#import "CategoryViewCell.h"
#import "AppService.h"
#import "AppsViewController.h"
#import "UIViewController+ACMethods.h"

NSString *const appsSegue = @"toApps";
@interface CategoriesViewController ()
@property (weak, nonatomic) IBOutlet UITableView *categoryTableView;
@property NSArray *categoriesArray;
@end

@implementation CategoriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.categoryTableView registerNib:[UINib nibWithNibName:CATEGORY_NIB_NAME bundle:nil] forCellReuseIdentifier:CATEGORY_CELL];
    [self setCategoriesViewNavigationBar];
    [self getCategories];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*!Get all categories from local*/
-(void)getCategories{
    [AppService fetchCategoriesFromLocal:^(NSArray<AppCategory *> *categories) {
        _categoriesArray = [categories mutableCopy];
    }];
}

#pragma mark-UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_categoriesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CATEGORY_CELL];
    AppCategory *category = [_categoriesArray objectAtIndex:indexPath.row];
    [cell setCategory:category];
    [cell configureCell];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.categoryTableView deselectRowAtIndexPath:indexPath animated:NO];
    [self performSegueWithIdentifier:appsSegue sender:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return DEFAULT_CELL_HEIGHT;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:appsSegue]) {
        NSIndexPath *indexPath = (NSIndexPath *)sender;
        
        // Get reference to the destination view controller
        AppsViewController *vc = [segue destinationViewController];
        AppCategory *category = [_categoriesArray objectAtIndex:indexPath.row];
        [vc setCategory:category];
        [vc setAllCategories: [_categoriesArray mutableCopy]];
    }
}


@end
